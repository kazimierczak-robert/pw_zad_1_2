// Zad 1.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include "list"
#include <iostream>
using namespace std;

DWORD WINAPI ThreadFunc(LPVOID lpdwParam) {
	Sleep(INFINITE);
	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	DWORD dwThreadId;
	list<HANDLE> lista;
	HANDLE ahThread;
	unsigned long long int i = 0;
	try {
		while (true)
		{
			ahThread = CreateThread(NULL, 0, ThreadFunc, 0, 0, &dwThreadId);
			lista.push_back(ahThread);
			//printf_s("Dodano nowy watek o numerze: %d\n", i);
			i++;
		}
	}
	catch (bad_alloc ex) {
		CloseHandle(ahThread);
		lista.clear();
		cout << "Out of memory! - maksymalna liczba watkow: " << i << "\n";
	}
	system("pause");
	return 0;
}